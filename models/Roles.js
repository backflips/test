var mongoose = require('mongoose');

var RoleSchema = new mongoose.Schema({
	title: String
});

module.exports = mongoose.model('Role', RoleSchema);
