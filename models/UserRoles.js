var mongoose = require('mongoose');

var UserRoleSchema = new mongoose.Schema({
	title: { type:mongoose.Schema.Types.ObjectId, ref: 'Role' },
	granted: Boolean,
	user: { type:mongoose.Schema.Types.ObjectID, ref: 'User'}
});

module.exports = mongoose.model('UserRole', UserRoleSchema);
